package com.buntplanet.gildedrose;

import java.util.Arrays;

public class App {
  public static void main(String[] args) {
    System.out.println("Hola mundo!");
    for (String arg : Arrays.asList(args))
      System.out.println(arg);

    /**
     Para parsear un JSON:

     File file = new File(path);
     JSONParser parser = new JSONParser();
     FileReader fr = new FileReader(file);
     Object obj = parser.parse(fr);
     JSONObject jsonObject = (JSONObject) obj;
     String content = (String) jsonObject.get("element");

     */

    /**
     Para parsear un XML:

     File file = new File(path);
     Document document =  builder.build(file);
     Element rootNode = document.getRootElement();
     String content = rootNode.getChildText("element");

     */
  }

}
